# node-rxjs-operators

A collection of additional RxJS operators.

Can be used by lifting the operator in the stream.

The library can be found [on NPM](https://www.npmjs.com/package/@toincrease/node-rxjs-operators)
