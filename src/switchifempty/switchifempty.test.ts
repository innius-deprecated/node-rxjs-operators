import {assert} from "chai";
import {Observable} from "@reactivex/rxjs"

import {switchIfEmpty} from "./switchifempty"

describe("switchIfEmptyOperator", () => {
    it("Should emit the original observable", (done) => {
        const source = [1, 2, 3];
        const other = [4];
        var seen = [];
        Observable
            .from(source)
            .let(switchIfEmpty(Observable.from(other)))
            .subscribe(
            (n) => seen.push(n),
            (e) => done(e),
            () => {
                assert.equal(3, seen.length);
                assert.deepEqual(source, seen);
                done();
            }
            );
    });
    it("Should be lazy", (done) => {
        const source = [1, 2, 3];
        const other = [4];
        var seen = [];
        var side_effect = false;
        Observable
            .from(source)
            .let(switchIfEmpty(Observable.from(other).map((x) => {
                side_effect = true;
                return x;
            })))
            .subscribe(
            (n) => seen.push(n),
            (e) => done(e),
            () => {
                assert.equal(3, seen.length);
                assert.deepEqual(source, seen);
                assert.isFalse(side_effect);
                done();
            }
            );
    });
    it("Should emit the other observable", (done) => {
        const source = [];
        const other = [4, 5];
        var seen = [];
        Observable
            .from(source)
            .let(switchIfEmpty(Observable.from(other)))
            .subscribe(
            (n) => seen.push(n),
            (e) => done(e),
            () => {
                assert.equal(2, seen.length);
                assert.deepEqual(other, seen);
                done();
            }
            );
    });
    it("Should be able to cope with different types", (done) => {
        const source = [1, 2, 3];
        const other = ["A", "B"];
        var seen = [];
        Observable
            .from(source)
            .let(switchIfEmpty(Observable.from(other)))
            .subscribe(
            (n) => seen.push(n),
            (e) => done(e),
            () => {
                assert.equal(3, seen.length);
                assert.deepEqual(source, seen);
                done();
            }
            );
    });
    it("Should be able to cope with other types", (done) => {
        const source: number[] = [];
        const other = ["A", "B"];
        var seen = [];

        const srcin: Observable<number> = Observable.from(source);
        const otherin: Observable<string> = Observable.from(other);

        srcin
            .let(switchIfEmpty(otherin))
            .subscribe(
            (n) => seen.push(n),
            (e) => done(e),
            () => {
                assert.equal(2, seen.length);
                assert.deepEqual(other, seen);
                done();
            }
            );
    });
});
