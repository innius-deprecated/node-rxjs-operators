import {Observable, Operator, Subscriber} from "@reactivex/rxjs";

/**
 * Returns an Observable that emits the elements of the source or switches to another observable if the original source is empty.
 * @param {any} other The other observable to switch to, if the source is empty.
 * @return {Observable} an Observable of the items emitted by the original observable or the other if the original is empty.
 */

export function switchIfEmpty<T, R>(other: Observable<R>): (source: Observable<T>) => Observable<T | R> {
    return (source: Observable<T>): Observable<T | R> => {
        return source.lift(new SwitchIfEmptyOperator<T, R>(other));
    };
}

/**
 * Returns an Observable that emits the elements of the source or switches to another observable if the original source is empty.
 * @param {any} other The other observable to switch to, if the source is empty.
 * @return {Observable} an Observable of the items emitted by the original observable or the other if the original is empty.
 */

class SwitchIfEmptyOperator<T, R> implements Operator<T, T | R> {
    constructor(private other: Observable<R>) { }

    call(subscriber: Subscriber<R>, source: any): Subscriber<T> {
        return source._subscribe(new SwitchIfEmptySubscriber(subscriber, this.other));
    }
}

class SwitchIfEmptySubscriber<T, R> extends Subscriber<T> {
    private isEmpty: boolean = true;

    constructor(destination: Subscriber<T | R>, private other: Observable<R>) {
        super(destination);
    }

    protected _next(value: T): void {
        this.isEmpty = false;
        this.destination.next(value);
    }

    protected _complete(): void {
        if (this.isEmpty) {
            this.other.subscribe(
                o => this.destination.next(o),
                o => this.destination.error(o),
                () => this.destination.complete()
            );
        } else {
            this.destination.complete();
        }
    }
}
